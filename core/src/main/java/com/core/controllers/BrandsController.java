package com.core.controllers;

import java.util.Arrays;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/brands")
public class BrandsController {

  @GetMapping
  public List<Object> index(){
    String url = "http://localhost:3001";
    RestTemplate restTemplate = new RestTemplate();

    Object[] brands = restTemplate.getForObject(url, Object[].class);

    return Arrays.asList(brands);
  }
}
