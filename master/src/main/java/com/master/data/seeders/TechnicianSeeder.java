package com.master.data.seeders;

import com.master.data.models.Technician;
import com.master.data.repositories.TechnicianRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class TechnicianSeeder implements CommandLineRunner {
	@Autowired
	TechnicianRepository TechnicianRepository;

  @Override
  public void run(String... args) throws Exception {
    initTechnicianData();
  }

  private void initTechnicianData() {
    if(TechnicianRepository.count() == 0){
      Technician technician1 = new Technician("Gunawan", "Samsung", "Android", 0);
      TechnicianRepository.save(technician1);

      Technician technician2 = new Technician("Bobby", "Xiaomi", "Android", 0);
      TechnicianRepository.save(technician2);

      Technician technician3 = new Technician("John", "Iphone", "IOS", 0);
      TechnicianRepository.save(technician3);

      Technician technician4 = new Technician("Roger", "Ipad", "IOS", 0);
      TechnicianRepository.save(technician4);

      Technician technician5 = new Technician("Satrio", "Oppo", "Android", 0);
      TechnicianRepository.save(technician5);
    }
    System.out.println("Technician data have " + TechnicianRepository.count() + " data");
  }
}
