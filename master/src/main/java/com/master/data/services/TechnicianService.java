package com.master.data.services;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import com.master.data.models.Technician;
import com.master.data.repositories.TechnicianRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class TechnicianService {
  @Autowired
  private TechnicianRepository technicianRepo;


  public List<Technician> findAllTechnician(){
    return (List<Technician>)technicianRepo.findAll();
  };

  public Technician create(Technician params) {
    return technicianRepo.save(params);
  }

  public boolean delete(long id) {
    try {
      technicianRepo.deleteById(id);
      return true;
    } catch (Exception e) {
      System.out.println(e.getMessage());
      return false;
    }
  }

  public Technician findById(long id) {
    Optional<Technician> result = technicianRepo.findById(id);
    if (result.isPresent()) {
      return result.get();
    } else {
      return null;
    }
  }

  public boolean update(Technician params) {
    try {
      technicianRepo.save(params);
      return true;
    } catch (Exception e) {
      System.out.println(e.getMessage());
      return false;
    }
  }
}
