package com.master.data.repositories;

import com.master.data.models.Technician;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TechnicianRepository extends CrudRepository<Technician, Long> {
}
