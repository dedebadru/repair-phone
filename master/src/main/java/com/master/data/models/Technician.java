package com.master.data.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity

@Table(name= "technicians")

public class Technician {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name="id")
  public Long id;

  @Column(name="name")
  public String name;

  @Column(name="specialist")
  public String specialist;

  @Column(name="platform")
  public String platform;

  @Column(name="queue")
  public int queue;

  public Technician(){}

  public Technician(String name, String specialist, String platform, int queue){
    this.name = name;
    this.specialist = specialist;
    this.platform = platform;
    this.queue = queue;
  }
}
