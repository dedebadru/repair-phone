package com.master.controllers;

import java.util.List;

import com.master.data.models.Technician;
import com.master.data.services.TechnicianService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController()
@RequestMapping("/technicians")
public class TechniciansController {
  @Autowired
  TechnicianService technicianService;

  @GetMapping("")
  public List<Technician> index() {
    return technicianService.findAllTechnician();
  }

  @GetMapping("{id}")
  public Technician getTechnician(@PathVariable long id) {
    return technicianService.findById(id);
  }

  @PostMapping("")
  public String addtechnician(@RequestBody Technician technician) {
    if(technician != null) {
      technicianService.create(technician);
      return "Added a technician";
    } else {
      return "Request does not contain a body";
    }
  }

  @DeleteMapping("{id}")
  public String deletetechnician(@PathVariable("id") long id) {
    if(id > 0) {
      if(technicianService.delete(id)) {
        return "Deleted the technician.";
      } else {
        return "Cannot delete the technician.";
      }
    }
    return "The id is invalid for the technician.";
  }

  @PutMapping("")
  public String updatetechnician(@RequestBody Technician technician) {
    if(technician != null) {
      technicianService.update(technician);
      return "Updated technician.";
    } else {
      return "Request does not contain a body";
    }
  }
}
