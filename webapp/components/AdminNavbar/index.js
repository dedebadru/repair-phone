import { Navbar, Nav, Container } from 'react-bootstrap';

export default function AdminNavbar() {
  return (
    <>
      <Navbar bg="light" variant="light">
        <Container>
          <Navbar.Brand href="/admin">Admin</Navbar.Brand>
          <Nav className="me-auto">
            <Nav.Link href="/admin">List Order</Nav.Link>
            <Nav.Link href="/admin/technician">List Teknisi</Nav.Link>
          </Nav>
        </Container>
      </Navbar>
      <br/>
    </>
  );
}
