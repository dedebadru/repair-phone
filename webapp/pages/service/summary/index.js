import { Table } from 'react-bootstrap';

export default function ServiceSummary() {
  return (
    <Table striped bordered hover>
      <tbody>
        <tr>
          <th>Teknisi</th>
          <td>Gunawan</td>
        </tr>
        <tr>
          <th>Antrian</th>
          <td>Urutan 10</td>
        </tr>
        <tr>
          <th>Lama Pengerjaan</th>
          <td>10 Menit</td>
        </tr>
        <tr>
          <th>Harga</th>
          <td>10.000</td>
        </tr>
      </tbody>
    </Table>
  );
}