import { Card, Form, Button } from 'react-bootstrap';

export default function Service() {
  return (
    <>
      <Card.Title className="text-center">Pendaftaran Service Handphone</Card.Title>
      <Card.Text>
        <Form action="/service/summary" method="POST">
          <Form.Group className="mb-3" controlId="service.Name">
            <Form.Label>Nama</Form.Label>
            <Form.Control type="text" placeholder="Nama lengkap" />
          </Form.Group>

          <Form.Group className="mb-3" controlId="service.Address">
            <Form.Label>Alamat</Form.Label>
            <Form.Control as="textarea" rows={3} />
          </Form.Group>

          <Form.Group className="mb-3" controlId="service.Email">
            <Form.Label>Email</Form.Label>
            <Form.Control type="email" placeholder="Email" />
          </Form.Group>

          <Form.Group className="mb-3" controlId="service.Handphone">
            <Form.Label>Nomor Handphone</Form.Label>
            <Form.Control type="text" placeholder="Nomor Handphone" />
          </Form.Group>

          <Form.Group className="mb-3" controlId="service.Brand">
            <Form.Label>Brand Handphone</Form.Label>
            <Form.Select>
              <option>-- Pilih Brand Handphone --</option>
              <option value="1">Samsung</option>
              <option value="2">Iphone</option>
            </Form.Select>
          </Form.Group>

          <Form.Group className="mb-3" controlId="service.Complaint">
            <Form.Label>Keluhan Kerusakan</Form.Label>
            <Form.Select>
              <option>-- Keluhan Kerusakan --</option>
              <option value="1">Mati Total</option>
              <option value="2">Layar Retak</option>
            </Form.Select>
          </Form.Group>

          <Button variant="primary" type="submit">
            Submit
          </Button>
        </Form>
      </Card.Text>
    </>
  );
}
