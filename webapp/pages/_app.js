import 'bootstrap/dist/css/bootstrap.min.css';
import '../styles/globals.css';

import { Container, Row, Col, Card } from 'react-bootstrap';
import Link from 'next/link';

function MyApp({ Component, pageProps }) {
  return(
    <Container className="layout-container">
      <Row>
        <Col>
          <Card>
            <Card.Header as="h4">
              <Link href="/">
                <a>ABC Phone</a>
              </Link>
            </Card.Header>

            <Card.Body>
              <Component {...pageProps}/>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  )
}

export default MyApp
