import { Card, Form, Button } from 'react-bootstrap';

export default function Status() {
  return (
    <>
      <Card.Title className="text-center">Status Service Handphone</Card.Title>
      <Card.Text>
        <Form action="/status/summary" method="POST">
          <Form.Group className="mb-3" controlId="service.Handphone">
            <Form.Label>Nomor Handphone</Form.Label>
            <Form.Control type="text" placeholder="Nomor Handphone" />
          </Form.Group>

          <Button variant="primary" type="submit">
            Submit
          </Button>
        </Form>
      </Card.Text>
    </>
  );
}
