import { Table } from 'react-bootstrap';

export default function StatusSummary() {
  return (
    <Table striped bordered hover>
      <tbody>
        <tr>
          <th>Teknisi</th>
          <td>Gunawan</td>
        </tr>
        <tr>
          <th>Status</th>
          <td>Menunggu</td>
        </tr>
        <tr>
          <th>Brand Handphone</th>
          <td>Iphone</td>
        </tr>
        <tr>
          <th>Kerusakan</th>
          <td>Boot Loop</td>
        </tr>
      </tbody>
    </Table>
  );
}