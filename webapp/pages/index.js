import { Row, Col } from 'react-bootstrap';
import Link from 'next/link';

export default function Home() {
  return (
    <Row className="text-center">
      <Col>
        <Link href="/service">
          <a className="btn btn-outline-primary">
            Service HP
          </a>
        </Link>
      </Col>

      <Col>
        <Link href="/status">
          <a className="btn btn-outline-info">
            Lihat Status Service
          </a>
        </Link>
      </Col>
    </Row>
  )
}
