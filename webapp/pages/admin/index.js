import { Table, Card } from 'react-bootstrap';
import AdminNavbar from '../../components/AdminNavbar';

export default function AdminIndex() {
  return (
    <>
      <AdminNavbar/>
      <Card.Title className="text-center">List Order Service Handphone</Card.Title>
      <Card.Text>
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>#</th>
              <th>Nama Customer</th>
              <th>Kerusakan</th>
              <th>Brand Handphone</th>
              <th>Nama Teknisi</th>
              <th>Durasi</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1</td>
              <td>Dandy</td>
              <td>Boot Loop</td>
              <td>Xiaomi</td>
              <td>Gunawan</td>
              <td>10</td>
            </tr>
            <tr>
              <td>2</td>
              <td>Suwadi</td>
              <td>Layar Retak</td>
              <td>Xiaomi</td>
              <td>Bobby</td>
              <td>10</td>
            </tr>
          </tbody>
        </Table>
      </Card.Text>
    </>
  );
}