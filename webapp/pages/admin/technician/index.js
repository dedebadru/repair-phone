import { Table, Card } from 'react-bootstrap';
import AdminNavbar from '../../../components/AdminNavbar';

export default function AdminTechnician() {
  return (
    <>
      <AdminNavbar/>
      <Card.Title className="text-center">List Teknisi Service Handphone</Card.Title>
      <Card.Text>
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>#</th>
              <th>Nama Teknisi</th>
              <th>Jumlah Service</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1</td>
              <td>Gunawan</td>
              <td>10</td>
            </tr>
            <tr>
              <td>2</td>
              <td>Bobby</td>
              <td>15</td>
            </tr>
            <tr>
              <td>3</td>
              <td>John</td>
              <td>20</td>
            </tr>
          </tbody>
        </Table>
      </Card.Text>
    </>
  );
}